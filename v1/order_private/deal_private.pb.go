// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/order_private/deal_private.proto

package order_private

import (
	order "gitlab.com/adhocguru/fcp/apis/gen/order/v1/order"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_v1_order_private_deal_private_proto protoreflect.FileDescriptor

var file_v1_order_private_deal_private_proto_rawDesc = []byte{
	0x0a, 0x23, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61,
	0x74, 0x65, 0x2f, 0x64, 0x65, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1a, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74,
	0x65, 0x1a, 0x15, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2f, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x13, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2f, 0x65, 0x6e, 0x75, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x19, 0x76,
	0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x5f, 0x64, 0x65,
	0x61, 0x6c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0x9f, 0x0a, 0x0a, 0x0b, 0x44, 0x65, 0x61,
	0x6c, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5f, 0x0a, 0x0f, 0x4c, 0x69, 0x73, 0x74,
	0x44, 0x65, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x12, 0x2a, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63,
	0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x67, 0x0a, 0x13, 0x4c, 0x69, 0x73,
	0x74, 0x44, 0x65, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x2e, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x50, 0x72,
	0x6f, 0x63, 0x65, 0x73, 0x73, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x73, 0x0a, 0x19, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x44, 0x65,
	0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x34, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x44, 0x65, 0x6c,
	0x69, 0x76, 0x65, 0x72, 0x79, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x71, 0x0a, 0x18, 0x4c, 0x69, 0x73, 0x74, 0x44,
	0x65, 0x61, 0x6c, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74,
	0x69, 0x6f, 0x6e, 0x12, 0x33, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61,
	0x6c, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69,
	0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x61, 0x0a, 0x10, 0x4c, 0x69,
	0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x2b,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x43, 0x61, 0x74, 0x65,
	0x67, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5b, 0x0a,
	0x0d, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x42, 0x72, 0x61, 0x6e, 0x64, 0x12, 0x28,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x42, 0x72, 0x61, 0x6e,
	0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69,
	0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5f, 0x0a, 0x0f, 0x4c, 0x69,
	0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x2a, 0x2e,
	0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44,
	0x69, 0x63, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x69, 0x0a, 0x14, 0x4c,
	0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x51, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x54,
	0x79, 0x70, 0x65, 0x12, 0x2f, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e,
	0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61,
	0x6c, 0x51, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74, 0x79, 0x54, 0x79, 0x70, 0x65, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x44, 0x69, 0x63, 0x74, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x64, 0x0a, 0x0d, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65,
	0x61, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x12, 0x28, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73,
	0x74, 0x44, 0x65, 0x61, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x29, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x4e,
	0x61, 0x6d, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x52, 0x0a, 0x07,
	0x47, 0x65, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x12, 0x22, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74,
	0x44, 0x65, 0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x23, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72,
	0x2e, 0x47, 0x65, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x55, 0x0a, 0x08, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x12, 0x23, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x24, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31,
	0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5e, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x44, 0x65,
	0x61, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x26, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x44,
	0x65, 0x61, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x27,
	0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x47, 0x65, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x61, 0x0a, 0x0c, 0x4c, 0x69, 0x73, 0x74, 0x44,
	0x65, 0x61, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x12, 0x27, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73,
	0x74, 0x44, 0x65, 0x61, 0x6c, 0x46, 0x69, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x28, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x76, 0x31, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x44, 0x65, 0x61, 0x6c, 0x46, 0x69,
	0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x5e, 0x0a, 0x1d, 0x63, 0x6f,
	0x6d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x5f, 0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0x50, 0x01, 0x5a, 0x38, 0x67,
	0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x64, 0x68, 0x6f, 0x63, 0x67,
	0x75, 0x72, 0x75, 0x2f, 0x66, 0x63, 0x70, 0x2f, 0x61, 0x70, 0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e,
	0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2f, 0x76, 0x31, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f,
	0x70, 0x72, 0x69, 0x76, 0x61, 0x74, 0x65, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var file_v1_order_private_deal_private_proto_goTypes = []interface{}{
	(*order.ListDealProcessRequest)(nil),           // 0: fcp.order.v1.order.ListDealProcessRequest
	(*order.ListDealProcessTypeRequest)(nil),       // 1: fcp.order.v1.order.ListDealProcessTypeRequest
	(*order.ListDealDeliveryConditionRequest)(nil), // 2: fcp.order.v1.order.ListDealDeliveryConditionRequest
	(*order.ListDealPaymentConditionRequest)(nil),  // 3: fcp.order.v1.order.ListDealPaymentConditionRequest
	(*order.ListDealCategoryRequest)(nil),          // 4: fcp.order.v1.order.ListDealCategoryRequest
	(*order.ListDealBrandRequest)(nil),             // 5: fcp.order.v1.order.ListDealBrandRequest
	(*order.ListDealProductRequest)(nil),           // 6: fcp.order.v1.order.ListDealProductRequest
	(*order.ListDealQuantityTypeRequest)(nil),      // 7: fcp.order.v1.order.ListDealQuantityTypeRequest
	(*order.ListDealNamesRequest)(nil),             // 8: fcp.order.v1.order.ListDealNamesRequest
	(*order.GetDealRequest)(nil),                   // 9: fcp.order.v1.order.GetDealRequest
	(*order.ListDealRequest)(nil),                  // 10: fcp.order.v1.order.ListDealRequest
	(*order.GetDealFileRequest)(nil),               // 11: fcp.order.v1.order.GetDealFileRequest
	(*order.ListDealFileRequest)(nil),              // 12: fcp.order.v1.order.ListDealFileRequest
	(*order.DictResponse)(nil),                     // 13: fcp.order.v1.order.DictResponse
	(*order.ListDealNamesResponse)(nil),            // 14: fcp.order.v1.order.ListDealNamesResponse
	(*order.GetDealResponse)(nil),                  // 15: fcp.order.v1.order.GetDealResponse
	(*order.ListDealResponse)(nil),                 // 16: fcp.order.v1.order.ListDealResponse
	(*order.GetDealFileResponse)(nil),              // 17: fcp.order.v1.order.GetDealFileResponse
	(*order.ListDealFileResponse)(nil),             // 18: fcp.order.v1.order.ListDealFileResponse
}
var file_v1_order_private_deal_private_proto_depIdxs = []int32{
	0,  // 0: fcp.order.v1.order_private.DealService.ListDealProcess:input_type -> fcp.order.v1.order.ListDealProcessRequest
	1,  // 1: fcp.order.v1.order_private.DealService.ListDealProcessType:input_type -> fcp.order.v1.order.ListDealProcessTypeRequest
	2,  // 2: fcp.order.v1.order_private.DealService.ListDealDeliveryCondition:input_type -> fcp.order.v1.order.ListDealDeliveryConditionRequest
	3,  // 3: fcp.order.v1.order_private.DealService.ListDealPaymentCondition:input_type -> fcp.order.v1.order.ListDealPaymentConditionRequest
	4,  // 4: fcp.order.v1.order_private.DealService.ListDealCategory:input_type -> fcp.order.v1.order.ListDealCategoryRequest
	5,  // 5: fcp.order.v1.order_private.DealService.ListDealBrand:input_type -> fcp.order.v1.order.ListDealBrandRequest
	6,  // 6: fcp.order.v1.order_private.DealService.ListDealProduct:input_type -> fcp.order.v1.order.ListDealProductRequest
	7,  // 7: fcp.order.v1.order_private.DealService.ListDealQuantityType:input_type -> fcp.order.v1.order.ListDealQuantityTypeRequest
	8,  // 8: fcp.order.v1.order_private.DealService.ListDealNames:input_type -> fcp.order.v1.order.ListDealNamesRequest
	9,  // 9: fcp.order.v1.order_private.DealService.GetDeal:input_type -> fcp.order.v1.order.GetDealRequest
	10, // 10: fcp.order.v1.order_private.DealService.ListDeal:input_type -> fcp.order.v1.order.ListDealRequest
	11, // 11: fcp.order.v1.order_private.DealService.GetDealFile:input_type -> fcp.order.v1.order.GetDealFileRequest
	12, // 12: fcp.order.v1.order_private.DealService.ListDealFile:input_type -> fcp.order.v1.order.ListDealFileRequest
	13, // 13: fcp.order.v1.order_private.DealService.ListDealProcess:output_type -> fcp.order.v1.order.DictResponse
	13, // 14: fcp.order.v1.order_private.DealService.ListDealProcessType:output_type -> fcp.order.v1.order.DictResponse
	13, // 15: fcp.order.v1.order_private.DealService.ListDealDeliveryCondition:output_type -> fcp.order.v1.order.DictResponse
	13, // 16: fcp.order.v1.order_private.DealService.ListDealPaymentCondition:output_type -> fcp.order.v1.order.DictResponse
	13, // 17: fcp.order.v1.order_private.DealService.ListDealCategory:output_type -> fcp.order.v1.order.DictResponse
	13, // 18: fcp.order.v1.order_private.DealService.ListDealBrand:output_type -> fcp.order.v1.order.DictResponse
	13, // 19: fcp.order.v1.order_private.DealService.ListDealProduct:output_type -> fcp.order.v1.order.DictResponse
	13, // 20: fcp.order.v1.order_private.DealService.ListDealQuantityType:output_type -> fcp.order.v1.order.DictResponse
	14, // 21: fcp.order.v1.order_private.DealService.ListDealNames:output_type -> fcp.order.v1.order.ListDealNamesResponse
	15, // 22: fcp.order.v1.order_private.DealService.GetDeal:output_type -> fcp.order.v1.order.GetDealResponse
	16, // 23: fcp.order.v1.order_private.DealService.ListDeal:output_type -> fcp.order.v1.order.ListDealResponse
	17, // 24: fcp.order.v1.order_private.DealService.GetDealFile:output_type -> fcp.order.v1.order.GetDealFileResponse
	18, // 25: fcp.order.v1.order_private.DealService.ListDealFile:output_type -> fcp.order.v1.order.ListDealFileResponse
	13, // [13:26] is the sub-list for method output_type
	0,  // [0:13] is the sub-list for method input_type
	0,  // [0:0] is the sub-list for extension type_name
	0,  // [0:0] is the sub-list for extension extendee
	0,  // [0:0] is the sub-list for field type_name
}

func init() { file_v1_order_private_deal_private_proto_init() }
func file_v1_order_private_deal_private_proto_init() {
	if File_v1_order_private_deal_private_proto != nil {
		return
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_order_private_deal_private_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_v1_order_private_deal_private_proto_goTypes,
		DependencyIndexes: file_v1_order_private_deal_private_proto_depIdxs,
	}.Build()
	File_v1_order_private_deal_private_proto = out.File
	file_v1_order_private_deal_private_proto_rawDesc = nil
	file_v1_order_private_deal_private_proto_goTypes = nil
	file_v1_order_private_deal_private_proto_depIdxs = nil
}
